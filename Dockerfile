# NOTES:
# * Ubuntu 19.04 will NOT work, it needs to be 18.04, there is a dbus incompatibility
# * You must have the nvidia-drm driver installed and loaded on the host, kubernetes installs wont have this by default
# * for nvidia hardware encoding, you need driver 418.74, the newer ones have a bug
# * you will need to connect via VNC and do the initial setup
FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /steam
ENV DISPLAY :0

RUN apt-get update && \
    apt-get install -y \
        x11vnc \
        pulseaudio \
        dbus-x11 \
        xinit \
        sudo \
        python-apt \
        kmod \
        pciutils \
        icewm  \
        software-properties-common \
        x11-xserver-utils \
        xterm

ARG nvidia_driver_version=418.74
ADD https://us.download.nvidia.com/XFree86/Linux-x86_64/${nvidia_driver_version}/NVIDIA-Linux-x86_64-${nvidia_driver_version}.run /NVIDIA.run
RUN chmod +x NVIDIA.run && \
    ./NVIDIA.run -q -s --no-kernel-module --no-unified-memory && \
    rm NVIDIA.run

RUN add-apt-repository -y ppa:alexlarsson/flatpak && \
    apt-get update && \
    apt-get install -y flatpak && \
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo && \
    flatpak install -y flathub com.valvesoftware.Steam

RUN useradd steam -s /bin/bash -m -G audio,video,voice -d /steam && \
    echo steam:steam | chpasswd && \
    mkdir /tmp/.X11-unix && \
    chmod 1777 /tmp/.X11-unix && \
    mkdir -p /var/run/dbus

RUN echo "load-module module-native-protocol-tcp auth-anonymous=1" >> /etc/pulse/default.pa && \
    echo "enable-shm=no" >> /etc/pulse/client.conf && \
    echo "high-priority=no" >> /etc/pulse/daemon.conf && \
    echo "realtime-scheduling=no" >> /etc/pulse/daemon.conf

ADD launch /launch
ADD xinitrc /xinitrc
ADD Xwrapper.config /etc/X11/Xwrapper.config
ADD asoundrc /asoundrc

CMD ["/launch"]